#include "file.h"

#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <sys/stat.h>
#include <sys/types.h>

#include <filesystem>
#include <functional>

namespace shdb {

File::File(const std::filesystem::path &path, bool create)
{
    int flags = create ? (O_CREAT | O_EXCL) : 0;
    int mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;
    fd = open(path.c_str(), O_DIRECT | O_SYNC | O_RDWR | flags, mode);
    if (fd == -1) {
        perror("Unable to open file");
        throw "Unable to open file";
    }
    size = discover_size();
}

File::~File()
{
    if (fd > 0) {
        close(fd);
    }
}

int File::get_fd() const
{
    return fd;
}

off_t File::get_size() const
{
    return size;
}

off_t File::get_page_count() const
{
    return (get_size() + page_size - 1) / page_size;
}

void File::read_page(void *buf, PageIndex index) const
{
    read(buf, page_size, page_size * index);
}

void File::write_page(void *buf, PageIndex index) const
{
    write(buf, page_size, page_size * index);
}

PageIndex File::alloc_page()
{
    PageIndex page_index = size / page_size;
    alloc(page_size);
    return page_index;
}

void File::read(void *buf, size_t count, off_t offset) const
{
    safe_syscall(count, [=] { return pread(fd, buf, count, offset); });
}

void File::write(void *buf, size_t count, off_t offset) const
{
    File::safe_syscall(count, [=] { return pwrite(fd, buf, count, offset); });
}

void File::alloc(off_t len)
{
    if (fallocate(fd, FALLOC_FL_ZERO_RANGE, size, len) == -1) {
        perror("Unable to allocate space to file");
        throw "Unable to allocate space to file";
    }
    size += len;
}

void File::safe_syscall(size_t count, const std::function<ssize_t()> &call) const
{
    for (auto r = call(); r != static_cast<ssize_t>(count); r = call()) {
        if (r == -1 && (errno != EINTR && errno != EAGAIN)) {
            perror("Unable to perform I/O");
            throw "Unable to perform I/O";
        }
    }
}

size_t File::discover_size() const
{
    struct stat statbuf;
    if (fstat(fd, &statbuf) == -1) {
        perror("Unable to get file stat");
        throw "Unable to get file stat";
    }
    return statbuf.st_size;
}

}    // namespace shdb
