package main

import (
	"encoding/json"
  "fmt"
	"io"
  "io/ioutil"
  "log"
	_ "net"
	"os"
	"os/exec"
	_ "os/signal"
	"strconv"
	_ "sync"
	"syscall"
	_ "time"
)

type TestSpec struct {
	Round []RoundSpec `json:"round"`
}

const kStateFile = "/tmp/state.db"

func startBinary(binaryName string, port int, done chan interface{}) int {
	filename := kStateFile
	p := exec.Command(binaryName, strconv.Itoa(port), filename)

	stderr, err := p.StderrPipe()
	if err != nil {
    log.Printf("Error creating stderr pipe: %s", err.Error())
  }
  err = p.Start()
  stderrDone := make(chan interface{})

	go func() {
	  var err error
    buf := make([]byte, 1024)
    var n int
    for err == nil {
      n, err = stderr.Read(buf)
      if n > 0 {
        fmt.Fprintf(os.Stderr, "child stderr: %s", string(buf[:n]))
      }
      if err != nil && err != io.EOF {
        log.Print("Error reading stderr: %s", err.Error())
      }
    }
    close(stderrDone)
  }()

	if err != nil {
		fmt.Printf("Unable to start subprocess: %s", err.Error())
	}

	go func() {
		p.Wait()
    if p.ProcessState.ExitCode() != 0 {
		  log.Fatalf("Exit code %d\n", p.ProcessState.ExitCode())
    }
    <-stderrDone
		close(done)
	}()

	return p.Process.Pid
}

func killProcess(pid int) {
	syscall.Kill(pid, syscall.SIGTERM)
}

func main() {
	binaryName := os.Args[1]
  port, err := strconv.Atoi(os.Args[2])
  specFilename := os.Args[3]

  if err != nil {
    fmt.Fprintf(os.Stderr, "Expected port number as a second argument")
    os.Exit(1)
  }

  specJson, err := ioutil.ReadFile(specFilename)

  if err != nil {
    fmt.Fprintf(os.Stderr, "Unable to read %s: %s\n", specFilename, err.Error())
    os.Exit(1)
  }

  var testSpec TestSpec
  err = json.Unmarshal(specJson, &testSpec)

  if err != nil {
    log.Fatalf("Unable to parse json test spec: %s", err.Error())
  }

  _, err = os.Stat(kStateFile)

  if err == nil {
    err = os.Remove(kStateFile)
    if err != nil {
      log.Fatalf("Unable to remove stale %s: %s", kStateFile, err.Error())
    }
  }

  m := createQueriesManager()

  for i, round := range(testSpec.Round) {
    processCompleted := make(chan interface{})
    pid := startBinary(binaryName, port, processCompleted)
    runRound(port, m, &round)
    killProcess(pid)
    <-processCompleted

    log.Printf("Completed round %d/%d\n", i, len(testSpec.Round))
  }

  err = m.validate()

  if err != nil {
    log.Fatalf("Validation error: %s", err.Error())
  }
}
