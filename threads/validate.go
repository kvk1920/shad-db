package main

import (
  "fmt"
  _ "log"
  "sort"
)

type validator struct {
  // Index on "writes", ordered by "end"
  writes []*writeQuery
  writesByEnd []int
}

func (self* validator) init(writes[]*writeQuery) {
  self.writes = writes
  self.writesByEnd = make([]int, len(writes))

  for i := 0; i < len(writes); i++ {
    self.writesByEnd[i] = i
  }

  sort.Slice(self.writesByEnd, func(i, j int) bool {
    return self.writes[self.writesByEnd[i]].i.end < self.writes[self.writesByEnd[j]].i.end
  })
}

// "writes" must be ordered by "begin"
func (self* validator) validate(r* readQuery)  error {
  start := int64(0)
  limit := r.i.end

  for pos := 0; pos < len(self.writes) && self.writes[self.writesByEnd[pos]].i.end <= r.i.begin; pos++ {
    if self.writes[self.writesByEnd[pos]].i.begin > start {
      start = self.writes[self.writesByEnd[pos]].i.begin
    }
  }

  searcher := func(arg int64) int {
    // sort.Search() return the first index i such that f(i) is true.
    return sort.Search(len(self.writes), func(index int) bool {
        return self.writes[index].i.begin >= arg
      })
    }

  // All writes with "begin" < limit and "end" > start interval may affect read "r"
  intlimit := searcher(limit)

  found := false
  for _, w := range self.writes[:intlimit] {
    if w.i.end <= start {
      continue
    }

    if w.value == *r.value {
      found = true
      break
    }
  }

  if !found {
    return fmt.Errorf("No write with matching value found for %v", r)
  }

  return nil
}
